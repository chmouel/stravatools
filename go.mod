module gitlab.com/chmouel/stravatools

go 1.21.5

require (
	github.com/go-chi/chi v1.5.5
	github.com/go-chi/httplog/v2 v2.0.8
	golang.org/x/oauth2 v0.15.0
)

require (
	github.com/go-chi/chi/v5 v5.0.10 // indirect
	github.com/golang/protobuf v1.5.3 // indirect
	golang.org/x/net v0.19.0 // indirect
	google.golang.org/appengine v1.6.7 // indirect
	google.golang.org/protobuf v1.31.0 // indirect
)
