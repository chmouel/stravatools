package main

import "gitlab.com/chmouel/stravatools"

func main() {
	if err := stravatools.NewServer(); err != nil {
		panic(err)
	}
}
