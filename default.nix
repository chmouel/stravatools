{ lib, buildGoModule, packageSrc ? ./. }:

buildGoModule rec {
  pname = "stravatools";
  version = "main";

  src = packageSrc;
  vendorHash = null;

  ldflags = [ "-s" "-w" ];

  meta = {
    description = "";
    homepage = "https://gitlab.com/chmouel/stravatools";
    license = lib.licenses.asl20;
    mainProgram = "stravatools";
  };
}
