package stravatools

import (
	"embed"
	"encoding/json"
	"fmt"
	"html/template"
	"io"
	"io/fs"
	"log/slog"
	"net/http"
	"os"

	_ "embed"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/httplog/v2"
)

var defaultHostPort = "localhost:8000"

//go:embed templates/static
var staticFiles embed.FS

//go:embed templates
var templatesFs embed.FS

// parse templateFS and add to router
var templateFS, _ = fs.Sub(templatesFs, "templates")

func httpGet(url, token string) (*http.Response, error) {
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, fmt.Errorf("http.NewRequest: %w", err)
	}
	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", token))
	return http.DefaultClient.Do(req)
}

func showTemplate(name string, r *http.Request, w http.ResponseWriter) {
	// parse templates
	templs, err := template.ParseFS(templateFS, name)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	strava_access_token := ""
	var strava_user *StravaAthlete
	if token_cookie, err := r.Cookie("strava_access_token"); err == nil {
		strava_access_token = token_cookie.Value
		resp, err := httpGet("https://www.strava.com/api/v3/athlete", strava_access_token)
		defer resp.Body.Close()
		if err == nil && resp.StatusCode == http.StatusOK {
			// read body
			resp_body, err := io.ReadAll(resp.Body)
			if err == nil {
				if err := json.Unmarshal(resp_body, &strava_user); err != nil {
					fmt.Printf("json.Unmarshal: %v\n", err)
				}
			}
		}
	}

	data := struct {
		StravaUser        *StravaAthlete
		StravaAccessToken string
	}{
		StravaUser:        strava_user,
		StravaAccessToken: strava_access_token,
	}
	if err := templs.ExecuteTemplate(w, name, data); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func NewServer() error {
	logger := httplog.NewLogger("strava-oauth", httplog.Options{
		JSON:     true,
		LogLevel: slog.LevelInfo,
	})

	// Service
	r := chi.NewRouter()
	r.Use(httplog.RequestLogger(logger))
	r.Use(middleware.Heartbeat("/ping"))

	r.Mount("/strava", NewStravaResource().Routes())

	staticFS, err := fs.Sub(staticFiles, "templates/static")
	if err != nil {
		return err
	}
	r.Handle("/static/*", http.StripPrefix("/static/", http.FileServer(http.FS(staticFS))))

	r.Get("/{page}", func(w http.ResponseWriter, r *http.Request) {
		page := chi.URLParam(r, "page")
		showTemplate(page+".html", r, w)
	})
	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		showTemplate("index.html", r, w)
	})

	hostPort := os.Getenv("STRAVATOOLS_HOST_PORT")
	if hostPort == "" {
		hostPort = defaultHostPort
	}
	logger.Info(fmt.Sprintf("Starting server on %s", hostPort))
	return http.ListenAndServe(hostPort, r)
}
