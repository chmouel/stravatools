package stravatools

type OauthInfo struct {
	AccessToken string `json:"access_token"`
}

type StravaAthlete struct {
	ID            int     `json:"id"`
	Username      string  `json:"username"`
	ResourceState int     `json:"resource_state"`
	Firstname     string  `json:"firstname"`
	Lastname      string  `json:"lastname"`
	Bio           string  `json:"bio"`
	City          string  `json:"city"`
	State         string  `json:"state"`
	Country       string  `json:"country"`
	Sex           string  `json:"sex"`
	Premium       bool    `json:"premium"`
	Summit        bool    `json:"summit"`
	CreatedAt     string  `json:"created_at"`
	UpdatedAt     string  `json:"updated_at"`
	BadgeTypeId   int     `json:"badge_type_id"`
	Weight        float64 `json:"weight"`
	ProfileMedium string  `json:"profile_medium"`
	Profile       string  `json:"profile"`
	Friend        *string `json:"friend"`
	Follower      *string `json:"follower"`
}

type StravaRoute struct {
	Athlete       StravaAthlete `json:"athlete"`
	Description   string        `json:"description"`
	Distance      float64       `json:"distance"`
	ElevationGain float64       `json:"elevation_gain"`
	ID            int64         `json:"id"`
	IdStr         string        `json:"id_str"`
	Map           struct {
		Id              string `json:"id"`
		SummaryPolyline string `json:"summary_polyline"`
		ResourceState   int    `json:"resource_state"`
	} `json:"map"`
	MapUrls struct {
		Url       string `json:"url"`
		RetinaUrl string `json:"retina_url"`
	} `json:"map_urls"`
	Name                string        `json:"name"`
	Private             bool          `json:"private"`
	ResourceState       int           `json:"resource_state"`
	Starred             bool          `json:"starred"`
	SubType             int           `json:"sub_type"`
	CreatedAt           string        `json:"created_at"`
	UpdatedAt           string        `json:"updated_at"`
	Timestamp           int           `json:"timestamp"`
	Type                int           `json:"type"`
	EstimatedMovingTime int           `json:"estimated_moving_time"`
	Waypoints           []interface{} `json:"waypoints"`
}
