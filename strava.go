package stravatools

import (
	"context"
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"html/template"
	"io"
	"net/http"
	"os"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/httplog/v2"
	"golang.org/x/oauth2"
)

var maxNumberRoutesToLoad = 30

type stravaResource struct {
	OAuthConfig *oauth2.Config
}

func NewStravaResource() *stravaResource {
	clientSecret := os.Getenv("STRAVATOOLS_CLIENT_SECRET")
	if clientSecret == "" {
		panic("STRAVATOOLS_CLIENT_SECRET not set")
	}
	clientID := os.Getenv("STRAVATOOLS_CLIENT_ID")
	if clientID == "" {
		panic("STRAVATOOLS_CLIENT_ID not set")
	}
	publicRouteURL := os.Getenv("STRAVATOOLS_PUBLIC_ROUTE_URL")
	if publicRouteURL == "" {
		panic("STRAVATOOLS_PUBLIC_ROUTE_URL not set")
	}
	return &stravaResource{
		OAuthConfig: &oauth2.Config{
			RedirectURL:  fmt.Sprintf("%s/strava/callback", publicRouteURL),
			ClientID:     clientID,
			ClientSecret: clientSecret,
			Endpoint: oauth2.Endpoint{
				AuthURL:  "https://www.strava.com/oauth/authorize",
				TokenURL: "https://www.strava.com/oauth/token",
			},
			Scopes: []string{"read"},
		},
	}
}

func (rs stravaResource) Routes() chi.Router {
	r := chi.NewRouter()
	r.Get("/login", rs.login)
	r.Get("/callback", rs.callback)
	r.Get("/routes/{userID}", rs.routes)
	return r
}

func generateStateOauthCookie(w http.ResponseWriter) string {
	expiration := time.Now().Add(20 * time.Minute)

	b := make([]byte, 16)
	_, _ = rand.Read(b)
	state := base64.URLEncoding.EncodeToString(b)
	cookie := http.Cookie{Name: "oauthstate", Value: state, Expires: expiration}
	http.SetCookie(w, &cookie)

	return state
}

func (rs stravaResource) login(w http.ResponseWriter, r *http.Request) {
	oauthState := generateStateOauthCookie(w)
	ret := rs.OAuthConfig.AuthCodeURL(oauthState)
	http.Redirect(w, r, ret, http.StatusTemporaryRedirect)
}

func (rs stravaResource) routes(w http.ResponseWriter, r *http.Request) {
	var strava_access_token string
	if token, err := r.Cookie("strava_access_token"); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	} else {
		strava_access_token = token.Value
	}
	userID := chi.URLParam(r, "userID")
	resp, err := httpGet(fmt.Sprintf("https://www.strava.com/api/v3/athletes/%s/routes?per_page=%d", userID, maxNumberRoutesToLoad), strava_access_token)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer resp.Body.Close()
	// read body
	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	var routes []StravaRoute
	if err := json.Unmarshal(respBody, &routes); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	// routes := []StravaRoute{
	// 	{
	// 		Name:     "Test Route",
	// 		Distance: 10000,
	// 		ID:       3176089545385164492,
	// 	},
	// 	{
	// 		Name:     "Hello Moto",
	// 		Distance: 20000,
	// 		ID:       3176081043542409932,
	// 	},
	// }

	funcmap := template.FuncMap{
		"toKM": func(meters float64) string {
			return fmt.Sprintf("%2.f", meters/1000)
		},
	}

	data := struct {
		Routes []StravaRoute
	}{
		Routes: routes,
	}

	tmpl, err := template.New("routes.html").Funcs(funcmap).ParseFS(templateFS, "routes.html")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if err := tmpl.Funcs(funcmap).ExecuteTemplate(w, "routes.html", data); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (rs stravaResource) callback(w http.ResponseWriter, r *http.Request) {
	cookie, _ := r.Cookie("oauthstate")
	logger := httplog.LogEntry(r.Context())
	if r.FormValue("state") != cookie.Value {
		logger.Error(fmt.Sprintf("invalid oauth state, expected '%s', got '%s'", cookie.Value, r.FormValue("state")))
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}
	_accessToken, err := rs.getUserDataFromStrava(r.Context(), r.FormValue("code"))
	if err != nil {
		logger.Error(fmt.Sprintf("failed to get user data: %s", err.Error()))
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}

	accessToken := &http.Cookie{
		Name:    "strava_access_token",
		Value:   _accessToken,
		Path:    "/",
		Expires: time.Now().Add(1000 * time.Hour),
	}
	http.SetCookie(w, accessToken)

	http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
}

func (rs stravaResource) getUserDataFromStrava(ctx context.Context, code string) (string, error) {
	token, err := rs.OAuthConfig.Exchange(ctx, code)
	if err != nil {
		return "", fmt.Errorf("code exchange wrong: %s", err.Error())
	}
	return token.AccessToken, nil
}
